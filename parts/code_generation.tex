In this part, we give details on how we transform the annotated AST
output by the middle-end into an intermediate code
representation. This pass is particular, because it actually contains
two sub-passes. The first one consists in generating a code in an
intermediate representation, and the second one modifies the obtained
code to make it readable by LLVM.

\section{LLVM}

In PFPL's compiler, we have chosen to use the compiler infrastructure
named LLVM (\emph{Low Level Virtual Machine}) which has many benefits:
it makes a lot of code optimization, it looks after the whole back-end
pass and it can generate a machine code for a big amount of
architectures (x86, PowerPC, ARM, etc). Nowadays, LLVM is recognized
and used by many compilers, for instance it is used by GHC (Haskell),
Clang (C, C++, Objective-C), Rubinius (Ruby).

There are two ways to communicate with LLVM in Haskell: use the
bindings or generate a code in LLVM's intermediate language called
LLVMIR (\emph{LLVM Intermediate Representation}). We have decided to
use the second way, we generate a LLVMIR code and give it to LLVM
which will then either execute it or generate an optimized assembly
code.

The main reasons why we use LLVM instead of building a homemade
back-end are :
\begin{prettylist}
  \item Unlike assembly languages, LLVMIR provides an unlimited number
    of registers.
  \item Optimization passes have been proven efficient, they will
    always be better than the one we could have code ourselves.
  \item It allows us to focus on the middle-end, we don't spend time
    learning assembly.
\end{prettylist}

The intermediate language is strongly typed, but the type system
remains simple. It has been designed in a way that allows the
front-end to generate code easily, and expressive enough to make
possible optimisations before machine deploy.

\section{LLVMIR, how it works?}

LLVMIR is a SSA (\emph{Static Single Assignment}) representation,
which means that a variable can be affected at most once. Variables
are split into versions, new variables are often indicated by the
orginal name with a number (the version), so that every variable in
the IR is unique.

Here's a simple example, to illustrate variables versioning:

PFPL code:
\begin{haskellcode}
  let x = 42; x := x + 2;
\end{haskellcode}

Intuitive IR:
\begin{haskellcode}
  %x = add i32 42, 0 x = add i32 %x, 2
\end{haskellcode}

Valid IR:
\begin{haskellcode}
  %x1 = add i32 42, 0 x2 = add i32 %x1, 2
\end{haskellcode}

We can see a problem coming, if a variable is modified within a
condition (in a \texttt{while} loop for instance), we have to be able
to know which version of the variable we will use after the condition,
if a variable is modified in a condition, its version will be then
modified, but if the condition isn't verified, the version is still
the same and we need a way to get the good version to make the code
run properly.

SSA representation has a recommended concept to solve this conflict,
called \emph{Phi node}. A Phi node is an instruction used to select a
value depending on the predecessor of the current block. We don't use
Phi nodes in our compiler, so we won't expand on them.

A way to pass through variable versioning is to use pointers for every
variable we create. When a variable is declared, a new pointer is
created, having the name of the variable, and every time the variable
is called in the code, the pointer is dereferenced to retrieve its
value. This is how we do in our compiler, it can seem pretty dirty,
but our aim isn't to make a powerful back-end, it really is to work
hard on the middle-end pass.

Our IR:
\begin{haskellcode}
  %x = alloca i32
  store i32 42, i32* %x
  
  %x1 = load i32* %x x2 = add i32 %x1, 2 store i32 %x2, i32* %x
\end{haskellcode}

On the one hand the code is bigger and we create plenty of pointers,
but on the other hand it is easier to generate because we don't have
to store versions of a variable. The LLVMIR code used to obtain the
value of a variable will always be the same in the program, this
implies an easier code generation algorithm.

\section{Code generation algorithm}

Now that LLVM and its intermediate representation have been
introduced, we will explain how we generate a LLVMIR code from an
annotated AST. The algorithm is divided into two parts. The first one
will generate linear code representation, which can yet be transmitted
to LLVM. The second corrects the code to let it be understandable by
LLVM.

\subsection{Linear code representation algorithm}

From the middle-end, we get an AST (which is a non-linear data
structure) and we will have to output a list of LLVMIR
instructions. There aren't many ways to obtain a linear representation
from a tree: we can either use a depth-first-search or a
breadth-first-search to ensure that every node in the tree is handled.

We here use a depth-first-search, by construction or the AST, if a
statement $A$ is called before a statement $B$ in a PFPL code, then
$A$ will be placed before $B$ in the depth-first-search, which is
exactly what we want to output a coherent LLVMIR code.

We use the depth-first-search the following way : when a node is
visited, its corresponding LLVMIR code will be appended to the code
generated in lower depths. The algorithm ensures to take care of every
node and to put them in the right order.

\subsubsection{Temporaries generation}

The last thing we have to do in the first part is to be able to
generate a LLVMIR code from an AST node. Each instruction has to be
accessible further in the code, that's why we have to find a suitable
way store them and the SSA form allows to use temporaries (or
temporary variables). A temporary variables starts with the $\%$
symbol and is viewed as a variable by LLVM. We need to generate
temporaries for every instruction written in the AST. Consider for
instance the folling LLVM code :

\begin{haskellcode}
  let x = 42 + 1337 * y;
\end{haskellcode}

As we explained it above, this instruction will generate a pointer to
an integer. But the value that will be stored in the pointer isn't
directly readable: it's an instruction too, we have to generate at
least one more temporary, whose content will then be store in the
pointer, containing the result of \texttt{42 + 1337 * y}, but we will
previously have to generate another temporary to store the result of
\texttt{1337 * y} and also temporaries to store \texttt{1337} and
\texttt{y}. The corresponding LLVMIR code is:
\begin{haskellcode}
  %t0 = 1337
  %t1 = %y
  %t2 = mul i32 %t0, %t1
  %t3 = 42
  %t4 = add i32 %t2, %t3
  
  %x = alloca i32
  store i32 %t4, i32* %x
\end{haskellcode}

We can see that our temporaries are generated this way : $\%tX$, with
$X$ starting from 0 and increasing every time a new temporary is
needed. If we were using a procedural language, the generation
wouldn't have been hard, but we use Haskell which is a purely
functional programming language: variables aren't mutable. We had to
find a way to generate fresh temporaries, considering the fact that we
may have already generated temporaries before, we have to handle an
index which will increase each time a temporary is generated.

Our solution is simple, every time a node is visited, the current
index is also received, the code uses this index to generate the
temporaries it needs, can pass it to higher depths, and at the end of
the visit, the new index is returned with the corresponding LLVMIR
code, so that the new index can be used forward in the
depth-first-search. This algorithm assures that a temporary isn't
declared twice (which would be impossible because SSA form denies it).

\subsection{Code correction}

The currently generated code isn't directly readable by LLVM, we can't
store values in temporaries, we can only store results of instructions
(such as \texttt{add} or \texttt{mul}). This is the reason why this
post-pass exists: to correct the code.

We can see in the code generated by the first part, that the only bad
thing is the use of temporaries to store values and intructions
instead of storing instructions only. The idea is to replace
temporaries' names by their values when they are needed, this is called
\textit{constants spreading}.

\subsubsection{Constants spreading}

At the end of the code generation, we have a list of instructions
(such as \texttt{\%t0 = 1337}) and we want to spread constants. A
constant can be either a number (integer or float), a boolean (true
or false) or an already-created temporary.

Let's say that we have \texttt{X = Y} in the instructions list, and Y
is a constant, so we would like to spread Y instead of using X in
further instructions, we will then be able to save a temporary. We
loop over the instructions remaining, if an occurence of X is found,
we replace it by Y. Once the end of the list has been reached, we can
remove the instruction \texttt{X = Y}.

For the code used before:
\begin{haskellcode}
  let x = 42 + 1337 * y;
\end{haskellcode}

Our compiler will output this LLVMIR code:
\begin{haskellcode}
  %t2 = mul i32 1337, %y
  %t4 = add i32 %t2, 42
  
  %x = alloca i32
  store i32 %t4, i32* %x
\end{haskellcode}


We now have a shortened and working code, and we can pass it to LLVM.
