The translation of a program from a language to another like the
translation between two idiomas: you first have to understand and
verify the structural (and semantic) correctness of the source text.
This is exactly what the front-end does. We shall remind that it's
splitted in three different phases which are
\begin{prettylist}
\item Lexical analysis
\item Syntax analysis
\item Semantic analysis
\end{prettylist}
In fact, the front-end is a analysis step, whereas the rest of the
compiler is more like a synthesis.

We will treat the semantic analysis in a different chapter, since it
contains typing, which is a big part of this whole work.

\section{Lexing}

The purpose of this step is to take a stream of characters and to produce
an output stream of names, numbers, keywords, and symbols. In a
nutshell, the lexer does this type of transformation on the source :

\begin{haskellcode}
*Main> lexer "function f(x :: Int) { let y = 1; return x + y; } -- Test"
[TokenFunction, TokenVar "f", TokenLeftPar, TokenVar "x", TokenType,
TokenTInt, TokenRightPar, TokenLeftCur, TokenLet, TokenVar "y", TokenDecl,
TokenInt 1, TokenSemiColon, TokenReturn, TokenVar "x", TokenPlus,
TokenVar "y", TokenSemiColon, TokenRightCur]
\end{haskellcode}
We see that white spaces, tabulations, comments are removed. The
keywords like \texttt{let} are turned into the more abstract -- and
semantic -- keyword \texttt{TokenLet}. This makes the future work on
the source easier, since all the useless details were removed and a
bit of abstraction has been introduced.

\subsection{A bit of theory}

Lexical analysis mainly deals with \emph{lexical tokens}, or simply
tokens. They are units in the grammar of our langage, and we can
classify them in a finite set of token types. Typically we can find
\begin{prettylist}
\item IDs : \texttt{foo}, \texttt{fibo}, \dots
\item Integers : \texttt{42}, \dots
\item Reals : \texttt{1.618}, \dots
\item The typing operator \texttt{::}
\item The \texttt{if} statement
\end{prettylist}
and so on. The alphabetical tokens like \texttt{if} are also called
\emph{reserved words} or \emph{keywords}. They are part of what one
cannot use as an identifier. It is important to remark that blanks
(newlines, tabs, spaces) are usually not tokens -- excepted in Python
for instance -- since they have no semantic nor syntaxic value. This
is the same for comments, for they are only used before compilation.

\subsubsection{Regular expressions}

Now that we know what we want to obtain (a token stream), and from
what we start (a raw source code), let's talk for a little while about
the mecanisms that allow us to do that efficiently.

The idea of parsing algorithms is to first get the regular
expression describing each token. A regular expression, hereafter
named regexp, is an alphanumeric characters sequence. Each regular
expression stands for a set (possibly infinite) of string. We define an
\emph{alphabet} $A$ as a finite set of character, a \emph{language}
over $A$ as a sequence of characters belonging to $A$, and finally a
regexp can be defined this way :
$$
\begin{aligned}
  r ::= & & \textbf{Regular expression} \\
  |\ & \varnothing &\text{empty language} \\
  |\ & \epsilon &\text{empty word} \\
  |\ & a &\text{character a} \in A \\
  |\ & rr &\text{concatenation} \\
  |\ & r|r &\text{alternative (or sum)} \\
  |\ & r^\star &\text{star} \\
\end{aligned}
$$

Think the star as the ``repetition'' operator : $r^\star =
\epsilon|rr^\star$.  For desambiguisation purpose, we allow the use of
parenthesis and brackets. We also define that the operator, in
increasing order of priority are the alternative, the concatenation,
and the star.

We said that we want to describe the tokens of our language with
regexps, now we can do it ! For example, the regexp for the identifier
\texttt{if} is simply $if$, the one for the signed integers is 
$$(-|\epsilon)(0|1|2|3|4|5|6|7|8|9)(0|1|2|3|4|5|6|7|8|9)^\star$$ and
we can even describe real numbers (in usual computer notation) :
$$(-|\epsilon)(0|1|2|3|4|5|6|7|8|9)(0|1|2|3|4|5|6|7|8|9)^\star.(0|1|2|3|4|5|6|7|8|9)^\star$$
We can see that regexps can grow a lot as the complexity of the
languages expressed increases. This is why some additional symbols are
defined :
$$
\begin{aligned}
  r^+ & = &rr^\star \\
  r? & = &r|\epsilon \\
  [a\ z] & = &a|b|\dots|z\\
\end{aligned}
$$
and then a integer is decribed by $-?[0\ 9]^+$. Quite cryptic, isn't it ?

\subsubsection{Finite automata}

Now that we know what is exactly each token, or equivalently what
shape can have each one, we are insterested in finding if a given
string $S$ has one of these shapes. In reality, the problem is bigger:
not only we don't know to what ``token type'' affiliate $S$, but $S$
is a concatenation of tokens, so we have to find the type \emph{and}
the position in $S$.

A certain class of mathematical and computational objects allows us to
realize that. They are called \emph{finite automata}. A finite
automaton gives an answer to the question ``For a finite word $W$,
does my word belong to a given language $A$ ?''. Formally, we can
adopt the following definition :

\begin{tdefin}{Finite automaton}
A finite automaton on an alphabet $A$ is a quadruplet $(Q, T, I, F)$
where
\begin{enum}
\item $Q$ is a finite set of states
\item $T \subseteq Q \times A \times Q$ is a set of transitions
\item $I \subseteq Q$ is a set of initial states
\item $F \subseteq Q$ is a set of terminal states
\end{enum}
\end{tdefin}

This is an example of a finite automaton on the alphabet ${a, b}$
which recognizes the language of words ending by $a$ (a such
language can be described by the regexp
$(a|b)^\star a$\footnote{Curious, isn't it ?}) :

\begin{center}
\begin{tikzpicture}[%
    >=stealth,
    node distance=2cm,
    on grid,
    auto
  ]
  \node[initial,initial text=,state] (0) {0};
  \node[state,accepting] (1) [right of=0] {1};
  
  \path[->] (0) edge[loop above] node {$a, b$} (0);
  \path[->] (0) edge node {$a$} (1);
\end{tikzpicture}
\end{center}
With $Q = \{0, 1\}$, $T = \{(0,a,0), (0,b,0), (0,a,1)\}$, $I = \{0\}$
and $F = \{1\}$.

Now we can formally define how an automaton can recognize (or accept)
if a word belongs to a language.

\begin{tdefin}{Acceptation}
Let $A$ be an alphabet. We say that a word $a_1a_2...a_n \in A^\star $ is
recognized (or accepted) by an automaton $(Q,T,I,F)$ if and only if
there exists a sequence $s_0, s_1, ..., s_n$ of states such that
\begin{enum}
\item $s_0 \in I$
\item $\forall i \in \left[1,n-1\right] , (s_{i_1},a_i,s_i) \in T$
\item $s_n \in F$
\end{enum}
\end{tdefin}

At this point, we can recognize a word, or reject it, but our language
is a bit more complex than that. Since it is the sum of the the
languages constituting our tokens (a word is either a number or the
keyword \texttt{function}, or \texttt{if}, and so on \dots), we just
have to get the finite automata that recognizes each token.

There is a technique to get the finite automata recognizing the
language described by a given regexp. We won't discuss it here, since
it is just algorithmic considerations. But we can see now how the
language can be treated : from the regexp describing the different
tokens, we get the automata which recognize each token, and our big
result automaton is simply the union of these ones.

A problem is still annoying us : how to split the character flux
between tokens ? The answer is simple. When the automaton consume the
flux, we keep in memory the last state that was final, and if the
automaton fail, we split at the point, and start again with the
remaining characters.




\subsection{Lexer generators}

It is usually very unconvenient to entirely rewrite a lexer. For
languages with sufficiently simple grammar (such as PFPL, or even
Python) there are softwares which take a grammar as an input, and
generate an appropriate lexer. They are called \textbf{lexer
  generators}. One of the most famous is Flex (in C or C++), and in
this work we use its Haskell fork: Alex.

The generated lexers can be more or less complex and completes, for
some there are several features that can be included in the output. A
generated lexer can have a token position tracking and/or an error
handling, which is useful for the user. It can also be modified to
accept more exotic languages (like C).

\section{Parsing}

The goal of this step is to include the notion of syntax. We try to
make the link between a source code and the grammar of the language.

\subsection{Abstract Syntax Tree}

Just after the lexer, the code is linear since it is under the form of
a token stream. But the idea here is to turn it into a tree (more
precisely a n-ary tree). This form will allow us to easily realize the
rest of the compilation, with good performance. Trivially, we win a
$log(n)$ factor in many operations.

The tree is specific: we want the atomic expressions to be in the
leaves (constants, variables), and both the operators and the language
builtins in the nodes.

Such a tree is called a \textbf{Abstract Syntax Tree}, because it
physically contains the syntax of the code it holds.

\subsection{Grammars}

We won't discuss here about language theory since we judge it too far
from our subject. But we can talk a little about the grammar of our
language.

Formally, a grammar is built with four objects:
\begin{prettylist}
\item A set of non-terminal symbols $V$.
\item A set of terminal symbols $A \neq V$.
\item An axiom $\mathcal{A} \in V$
\item A set of production rules $P \subset V \times (V \cup A)^n, n \in \mathbb{N}^\star $
\end{prettylist}

If you take a look at the grammar in appendix, you will immediatly see
what each object is. This is called the Backus-Naur Form, for
instance:

$$S ::= b | Sc \iff V = \{S\}, A = \{b, c\}, \mathcal{A} = S$$

The $|$ means that there are two production rules with the same left
symbol. This way, our language is completely defined, and a "symbols"
stream comes from the lexer (namely, the tokens). We now have to
algorithmically turn the stream into a tree.

\subsection{Parsing tools}

Like the lexer, a certain class of tools permits us to generate a
suitable parser for our language. Yacc (the most famous, for C or C++)
can do that, and so does Happy, the Haskell fork of Yacc.

The idea is quite similar to the previous tool: it takes in input the
grammar under the BN form, and returns us a ready-to-use parser.

This parser takes himself a token stream, and returns an AST
representing our source code, indicating if there are (and where they
are) syntaxic errors.

Here is an example of the output of the parser, with the same source
code as the lexer part:

\begin{haskellcode}
*Main> parser . lexer $ "function f(x :: Int) :: Int 
    { let y = 1; return x + y; }"
Function "f" [("x",Int)] Int 
  (Seq 
    (Decl "y" (A (CstInt 1))) 
    (Return (Just (A 
      (Plus 
        (Var "x") 
        (Var "y")
      )
    )))
  )
\end{haskellcode}


