During the front-end, several checkings were performed on our code to
verify its correctness. We will now introduce another check we do to
see if it is coherent. This part is called \textbf{typechecking}

\section{Why types are importants}

Let's take the example of an addition: it is obvious that we can't add
an integer and a boolean, this is a nonsense. In this part we will see
why types are importants, and which ways of typing do exist.

Types have been introduced in compilers to have more safety in what we
do. A human knows that an integer can't be added to a boolean, but how
can our computer know that ? The intuitive idea is that an integer is
not the same thing than a boolean, so to each value in the code, we
associate a label, which will be its \textbf{type}. As soon as a
variable is declared, we define that type it belongs to. The last step
is to set up a pass that will check which verifies that each operation
is applied on variables having the good type, and if it hasn't the
good type, the compiler mentions an error.

We can say that types give sense to the language, we can bring
informations in the code to te computer, this increases its knowledge,
and the more informations we give, the better the computer can verify
their coherence.

Another advantage of types is that we don't have to read the whole
code of a function to understand what it consists in. For instance, if
we have a function \texttt{tolower}, we don't know want will be
lowered: a string, a character, a list of strings, ... Without types,
the only way to know is to read and understand the code (or to read
the doc) of the function.  With types, \textbf{prototypes} have been
introduced, and if the function \texttt{tolower} lowers strings, its
prototype will be: \texttt{string tolower(string)}. It says that this
function takes a string as parameter and returns another string. Types
are useful to help the computer, but they also help programmers to
know what functions do.

There are various ways of typing, but the two main are dynamic typing
and static typing. The difference lies in the moment when
verifications are performed: while operations are executed, everytime
they are executed or only once, before running the program?

\subsubsection{Dynamic typing}

The principle of dynamic typing is to type expressions only when
needed, during the execution of the program: it is executed, and we
verify that the type matches at each operation. As a consequence,
functions can have various types of return, according to the context.
One of the best known (and most used) language with dynamic typing is
\texttt{Python}. An exemple of a function with different return types
in Python:

\begin{pythoncode}
  def foo(cond):
    if cond:
      return 1
    else:
      return 'I am a string'
\end{pythoncode}

Here's the output for two different cases:
\begin{verbatim}
    >>> foo(True)
     1
    >>> foo(False)
     'I am a string'
\end{verbatim}

This typing is easy to understand and to set up, but one of its
disadvantages is that to be sure that all types in the program are
correct, we need to test all cases in the code. An error can occur in
a case but not in another, so if everything isn't tested, there can
still have remaining errors in the code.

\subsubsection{Static typing}

A language is statically typed if, before being executed, the code is
passed through a typechecking step (often performed on compile-time,
like in PFPL compiler, but some interpreters use a static typing). We
can cite for example the language \texttt{C}, which is statically
typed and inspired most of existing statically typed languages. The
advantage of static typing is that it alerts us of type errors (or
warnings) before running the program, once the typechecking has been
done, the compiler can get rid of types informations, which implies
that the code can run faster than a code written for a dynamically
typed interpreter which must keep many informations on typing
throughout the program.

\section{PFPL's type system and typechecking algorithm}

The syntaxic analysis outputs a non-annotated AST (i.e. with no
information on variables and functions). We will create variable
environments which will allow us to typecheck the code and avoid
prospective variable errors (e.g. redefining an existing variable).

This pass performs not only the typechecking, it also ensures:
\begin{prettylist}
\item The respect of variables scopes;
\item Some other static verifications (e.g. is there a \texttt{return} in every function?).
\end{prettylist}

An environment is an associative map that associates a type to each
variable or function. It is obvious that there can't be two variables
of the same name in a same environment. In PFPL, only functions ans
sub-blocks (\texttt{if, while, case of}) own an associatad sub-environment. The
environment a variable belongs is called its \textbf{scope}.

The algorithm is the following:

\begin{prettylist}
\item A first environment is created, containing all the functions in
  the code (and their types). It checks if parameters aren't
  repeated. This function won't be accepted (double x):
  \begin{luacode}
    function f(x :: Int, x :: Int) :: Int { ... }
  \end{luacode}

\item Once function's environment has been constituted, an environment
  is created for each function, they will contain the variables
  declared in these functions;

\item The AST is recursively traveled, each recursive call returns the
  environment of the left son (which will be modified if a new
  variable is declared) and the left son annotated with the types of
  the expressions.
\end{prettylist}

Apart from that, the algorithm verifies if the following rules are
respected. They must be read like this: everything over the line is
called \textbf{premises}, and everything under the
\textbf{conclusions}. A rule means that if we have the premises, then
we have the coonclusion. Let $x : \tau$ mean "the type of $x$ is
$\tau$", an environment is denoted $\Gamma$ and $\Gamma (x)$ is the
type of $x$ in $\Gamma$. We will write $E \Rightarrow [\Gamma,~E']$ to
mean "E returns the modified (or not) environment $\Gamma$ and the
annotated AST E'". $\Gamma + x : \tau$ is the same as
$\Gamma \cup \{x : \tau \}$ and
$$\Gamma + \Delta = \Gamma + \sum_{i \in \Delta -  \Gamma}i : \Delta (i)$$

\begin{mathpar}
\infer{\text{let } x = e \Rightarrow [\Gamma + x : \tau, \text{let } x = e]}
      {e : \tau}
\and
\infer{x \text{ := } e \Rightarrow [\Gamma, x \text{ := } e]}
      {e : \Gamma(x)}
\and
\infer{\text{if } b \text{ then } E \text{ else } F 
  \Rightarrow [\Gamma, \text{if } b \text{ then } E' \text{ else } F']}
      {b : \text{ bool} \and
        E \Rightarrow [\Gamma', E'] \and
        F \Rightarrow [\Gamma'', F']}
\and
\infer{\text{while } b \text{ do } E 
  \Rightarrow [\Gamma, \text{while } b \text{ do } E']}
      {b : \text{bool} \and E \Rightarrow [\Gamma', E']}
\and
\infer{\text{case } cond \text{ of } \{c_i \rightarrow E_i | i \in [1, n]\}
\Rightarrow [\Gamma, \text{case } cond \text{ of } \{c_i \rightarrow E_i' | i \in [1, n]\}}
      {cond : \tau \and
        n \in \ens{N^\star} \and
        \forall i \in [1, n], c_i : \tau \and 
        \forall i \in [1, n], E_i \Rightarrow [\Gamma_i, E_i']}
\and
\infer{\text{print } E \Rightarrow \text{print } E'}
      {E \Rightarrow [\Gamma', E']}
\and
\infer{E; F \Rightarrow [\Gamma'', E'; F'] }
      {E \Rightarrow [\Gamma', E'] \and 
        F \Rightarrow [\Gamma'', F'] 
        \text{ taking $\Gamma'$ as current env.}}
\and
\infer{\text{return } e \Rightarrow [\Gamma, \text{return } e]}
      {f : (\alpha, \beta, \dots) \rightarrow \tau \and e : \tau}
\end{mathpar} 

We can see that:
\begin{prettylist}
\item The only language structure that modifies an environment is
  \texttt{let}, which inserts a variable in the current environment and
  throws an exception if this variable is already defined.
\item Variable assigment (:= operator) throws an exception if the new
  value hasn't the same type as the old one.
\item For \texttt{if, while, case of} structures, a sub-environment is
  created (a copy of the current environment), but all the
  modifications on this sub-environment will be lost beyond the
  structure (e.g. a variable defined in a \texttt{while} block won't
  be accessible anywhere, except in this block).
\item If a rule isn't respected, an exception is thrown. This also
  applies if a variable is not found in the current environment.
\end{prettylist}

Previous rules don't include expressions' typechecking, we will now
see how it works. PFPL types ar given by the following grammar:

$$
\begin{aligned}
  typ ::= & & \textbf{Type} \\
  |\ & Int &\text{integer} \\
  |\ & Float &\text{real} \\
  |\ & Bool &\text{boolean} \\
  |\ & (typ, typ, \dots) \rightarrow typ &\text{function} \\
\end{aligned}
$$

We have another set of rules, defining how expressions have to be
typed. Our algorithm also verifies these rules (in addition to
previously seen rules). Let \textbf{Num} = \{Int, Float\} be the set
of arithmetic types.

\begin{mathpar}
\infer{x : \Gamma (x)}{}
\and
\infer{e_1\ op \ e_2 : \tau_1}
      {op \in \{+, -, *, /, \%\} \and 
        e_1 : \tau_1 \in \textbf{Num} \and 
        e_2 : \tau_2 \in \textbf{Num} \and
        \tau_1 = \tau_2}
\and
\infer{e_1\ op \ e_2 : \text{bool}}
      {op \in \{and, or\} \and 
        e_1 : \text{bool} \and 
        e_2 : \text{bool}}
\and
\infer{e_1\ op \ e_2 : \text{bool}}
      {op \in \{<, >, \leq, \geq, =, \neq\} \and 
        e_1 : \tau_1 \in \textbf{Num} \and 
        e_2 : \tau_2 \in \textbf{Num} \and
        \tau_1 = \tau_2}
\and
\infer{not\ e : \text{bool}}
      {e : \text{bool}}
\end{mathpar}



\section{Opening on types (or why types are so cool)}

As we just saw, types are a good mean to statically verify if our code
will work. But there are a lot more things that we can do with types.

It has been proved years ago that types are closely related to logic,
and in fact (through the Curry-Howard isomorphism) every term in type
theory is in correspondence with a term in logic (a proof
term). Basically, we are talking about this correspondence:
\begin{align*}
Value &\iff Proof \\
Type &\iff Theorem
\end{align*}
The Curry-Howard isomorphism allows us to manipulate complex proofs by
expressing them in a functional language with a sufficiently powerful
type system. Coq, Isabelle, Agda are good examples of how powerful
types and some theory on paper can accomplish.

There are still many things that we can do with types, like performing
static analysis, ensuring properties on algorithms before run-time,
encoding natural integers, regularizing access to memory (see the
Mezzo language, by the INRIA).
