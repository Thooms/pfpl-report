When one wants to create a program which has to be executed by a
computer, he has to write its source code. Such a code is a sequence
of instructions which will be read by the computer, and then executed.
However, a source code is not written in a ``conventional'' language,
like English or French, but in a ``programming language''. These are
thus the link between one wants to do, and what a computer will
actually do.

The step that translates a source code into real computations is
called \textbf{compilation} and the program which does the compilation
is naturally called a \textbf{compiler}. We now have to make a clear
difference between a language, which roughly is a set of words along
with a set of syntaxic rules, and an implementation of this language.
These two faces of a ``programming language'' involves a lot of
researches: in a hand, there are computer scientists who work on how
should be the languages of the future, or how should exactly behave a
program. In the other hand, they work on how to implement these
languages efficiently or to make them work on many platforms for
example.

Let's have a look at the implementation side: there are several ways
to treat a source code in order to make computations.
\begin{prettylist}
\item Native compilation : the source code is transcripted in assembly
  code\footnote{Assembly code represents a code which is extremely
    close to the machine, but still readable by a human.} (ASM) which
  is then assembled in machine code. For instance, gcc (C), g++ (C++)
  or GHC (Haskell) are native compilers.
\item Interpretation : the source code is not translated in ASM, but
  read by another program called interpreter, whose role is to execute
  the instructions directly from the code. Among interpreters, we can
  cite GHCi (Haskell) or the OCaml toplevel.
\item Source-to-source compilation : the source code is translated in
  another programming language, to be then compiled or
  interpreted. This is also used to adapt some code to exotic
  architectures. Js\_of\_ocaml can for exemple give us a Javascript
  code from an OCaml code.
\item Semi-compilation : the source code is translated to an
  intermediate language often called bytecode, which can be executed
  on a Virtual Machine. CPython, the main program used to execute
  python works this way, generating \texttt{.pyc} bytecode files which
  will then be executed. The advantage of this method is that a
  program can be executed on many platforms (given that the VM is
  available on them).
\item Just-in-Time (JIT) compilers : the idea here is to combine the
  advantages of the bytecode and the native compilation. The source
  code is translated in real-time to bytecode, and then to machine
  code (again in real-time). For instance, we can cite GNU CLISP
  (LISP) as a JIT compiler.
\end{prettylist}

The main goal of our work is to design and implement a (native)
compiler in Haskell for an imperative language we created, called
PFPL. The compiler is able to treat a PFPL source code, and performs
several checking, on types or dimensions. It uses LLVM\cite{LLVM} to
produce ASM code. The idea is to have a minimalistic but
functional\footnote{which means here useable.} compiler.

Haskell is a purely\footnote{well, almost.} functional and
research-oriented language, built on a call-by-need semantic model
(which means that expressions are evaluated in a lazy way: only when
their values are needed) which allows for exemple the use of infinite
structures.

We also use some classic compilation tools, to enforce the correctness
of our compiler, and to concentrate on less trivial steps in the
compilation (writing a parser is not exactly fun in this context).

The rest of this document is organized this way: we first introduce
more precisely PFPL, then we talk about the general organization of a
compiler, and in what way our compiler follow this organization. The
first steps of compilation are briefly explained in an other part. The
last three parts are constituted of our main work on this project:
typechecking, dimension checking, and code generation. An exhaustive
grammar of PFPL is available in appendix.
