PARTS=$(wildcard parts/*.tex)

all: report

report: report.tex $(PARTS)
	pdflatex -shell-escape report.tex
	bibtex report
	pdflatex -shell-escape report.tex
	pdflatex -shell-escape report.tex

.PHONY: clean mrproper

clean: 
	rm -rf *.aux *.bbl *.blg *.log *.out *.toc

mrproper: 
	rm -rf *.pdf